package com.formation.services;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.formation.models.Auteurs;

public class AuteurManager {
	
	List<Auteurs> listeAuteurs = new ArrayList<>();
	
	//la methode  qui va importer ce qui a dans listeAuteurs.txt
	
	public void load() {
		
		
		
		//R�cup�re le fichier
		try {
			File file = new File("./data/listeAuteurs.txt"); 
			
			//lit caract�re par caract�re
			FileReader fr = new FileReader(file);
			
			
			// Lit par bloc (la ligne enti�re jusqu'au retour � la ligne)
			BufferedReader br = new BufferedReader(fr);
			
			String ligne;
			
			
			while ((ligne = br.readLine()) != null) {
				
			      //System.out.println(ligne.split("#"));
				
					String parties[] = ligne.split("#");
					
					Auteurs a = new Auteurs(parties[0], parties[1], parties[2]);
					System.out.println(a);
					listeAuteurs.add(a);
					
			}
			br.close();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
	//Cr�er la methode pour trier la liste des auteurs
	public void traitement() {
		
		
	}
	
	//Methode pour exporter un fichier apr�s le traitement 
	//en fin de ompte le traitement a �t� fait dans cette methode
	public void export() {
		
		Collections.sort(listeAuteurs, new Comparator<Auteurs>() {

			@Override
			public int compare(Auteurs o1, Auteurs o2) {
				return o1.getDateDeNaissance().compareTo(o2.getDateDeNaissance());
				
			}
		});
		
		Collections.reverse(listeAuteurs);
		
		try {
			FileWriter myWriter = new FileWriter("./data/listeAuteursTriee.txt");
			
			for (Auteurs auteur : listeAuteurs) {
				myWriter.write(auteur.toString()+System.lineSeparator());
			}
			
			
			myWriter.close();
			System.out.println("Successfully wrote to the file.");
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("An error occurred.");
			e.printStackTrace();
			
		}
		
		
		
		
		
	}

}
