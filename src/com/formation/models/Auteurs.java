package com.formation.models;


import java.text.ParseException;
import java.text.SimpleDateFormat;
//import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class Auteurs /*implements Comparable<Auteurs> */{
	private String nom;
	private String prenom;
	private Date dateDeNaissance;
	
	

	public String getNom() {
		
		
		
		/**
		 * Autre Methode
		 */
		
		return nom.substring(0,1).toUpperCase()+nom.substring(1).toLowerCase();
		
		/**
		 * Cr�er une methode ici pour convertir le nom
		 * (1ere lettre Maj /le reste en Miniscule)
		
		
		String[] ary = nom.split("");
		
		StringBuffer sb = new StringBuffer();
		
		String premierCarectere = ary[0].toUpperCase();
		
		sb.append(premierCarectere);
		
	
		
		for(int i = 1; i<ary.length; i++) {
			
			
			sb.append(ary[i].toLowerCase());
			
			
		}
		
		
		
		
		return sb.toString();
		 */
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	
	

	@Override
	public String toString() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return getNom()+"#"+prenom+"#"+simpleDateFormat.format(dateDeNaissance);
	}

	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Auteurs other = (Auteurs) obj;
		return Objects.equals(dateDeNaissance, other.dateDeNaissance) && Objects.equals(nom, other.nom)
				&& Objects.equals(prenom, other.prenom);
	}
	
	
	//methode qui va me convertir la String en Date
	public Date convertDate(String dateDeN) {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
		
		Date date;
		try {
			date = simpleDateFormat.parse(dateDeN);
		} catch (ParseException e) {
			date = new Date();
		}
		return date;
	}

	
	public Auteurs(String nom, String prenom, String dateDeN) {
		super();
		
		
		this.nom = nom;
		this.prenom = prenom;
		this.dateDeNaissance = convertDate(dateDeN); 
	}
/*
	@Override
	public int compareTo(Auteurs autre) {
		
		return this.dateDeNaissance.compareTo(autre.getDateDeNaissance());
	}
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	

}
